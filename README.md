# Docker Evolution

From local build to production environment

## Date: do 24 june

## Blurb

Docker Evolution in short:
Your first step starts on your local machine, create your first image and run it. Next you want to run multiple containers and after that you want to run so many containers that you need mutliple vm's. Hence you need a container orchestrator.
In this session we will walk trough this evolution process which everybody need to go trough who wants to run containers in production.

## Subjects to cover

- [x] recap
- [x] Robin: Dockerfile best practices
- [x] Robin: resource limits
- [x] Bart: networking
- [x] Bart: storage/volumes
- [ ] build packs demo?

- [x] Bart: Docker - what is it and what not
- [x] Bart: Overzicht container orchestrators
  - Swarm, Nomad
  - Kubernetes (Rancher, Openshift, EKS, AKS, GKE)

- [x] why containers?
  - 12 factor app
  - build, ship, run
  - less dependecies (to other teams/infra)
  - self service: devops team owns the container

## Docker PPT - Docker Deep Dive

[Docker PPT - Docker Deep Dive](./ppt.md)

## Old Docker PPT - CINQ Connect

[Docker PPT from CINQ Connect](./CINQ-Connect-Containers-101.pptx)

## Exercises

[Exercises](./exercises/exercises.md)