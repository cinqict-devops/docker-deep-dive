
# Exercises

## 1. Docker as a tool

Single container can already be very powerful. Go to [tool](./tool/README.md) to see why.

## 2. Persistent volume

Start a Wordpress web page and [see how your data survives a restart of the container](./wordpress/README.md).

## 3. Compose to the max

Deploy a complex micro services example: [vote app](./voting-app/README.md). 
