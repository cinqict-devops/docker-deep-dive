# Vote App

The vote app is a simple distributed application running across multiple Docker containers.

## a. Check `docker-compose.yml`

View `docker-compose.yml` file and take a closer look at the:

- different networks
- depend_on conditions
- healthchecks
- "image:" or "build:" keys

## b. Run vote app

Run `docker-compose up -d`. Some images will be build, other will be downloaded from dockerhub. Hence it will take a while before everything is running.

When all services are up, run `docker ps` to view all running containers.

## c. Browse

Now on port 5000 the vote app is exposed.
And on port 5001 the result app is exposed.

For more info about this vote app:

- [Architecture](https://github.com/dockersamples/example-voting-app#architecture)
- [How it works](https://github.com/dockersamples/example-voting-app#notes)
