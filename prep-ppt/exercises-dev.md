
# Exercises

Yo, 10 juni wil ik een Docker vervolg workshop geven. Format: uurtje praten, uurtje opdrachten doen. Wil er iemand helpen met het maken van de opdrachten? Hoeft niet zo mooi als een "capture the flag", maar het is wel leuk om er wat aandacht aan te besteden.

De agenda van het praatje wordt iets als (moet nog wat snoeien om het binnen het uur te houden):

- Recap
- Dockerfile - Best Practices
- Container Registry
- Run single container
- Multiple containers (+networking +storage +resource limits)
- Multiple nodes (Container Orchestrators)
- Docker - what is it and what not

## Omgeving - Play with Docker 0f Azure?

Azure devtest lab:
- https://docs.microsoft.com/en-us/azure/devtest-labs/devtest-lab-add-vm
- https://docs.microsoft.com/en-us/azure/devtest-labs/devtest-lab-create-custom-image-from-vm-using-portal

Provision VM met:

- Docker CE
  - without sudo
- Docker Compose
  - tab completion
- this repo?

```shell
# Install Docker
# - Docker CE installed by azure
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
docker --version
docker run hello-world

# Install Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
sudo curl \
    -L https://raw.githubusercontent.com/docker/compose/1.29.2/contrib/completion/bash/docker-compose \
    -o /etc/bash_completion.d/docker-compose
source ~/.bashrc

```

## Excersises vorige keer

https://github.com/pve84/cinqconnect/blob/main/README.md

## Opdrachten

- `docker run image` als tool alias
- wordpress + db met compose 
  ref: https://docs.docker.com/samples/wordpress/
- voting app, compose to the max 
  ref: https://github.com/dockersamples/example-voting-app
