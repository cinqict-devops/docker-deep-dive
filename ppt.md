
# PPT

Remarks and keywords in addition to the PPT.

## Agenda

## Prerequisite

Make shell ready

```shell
# Clean system
docker system prune --all --volumes --force
docker image list

#  Project dir
cd /home/cornet/code/prive/docker-deep-dive/figlet

clear
```

## Recap

### Why?

- In addition to Roy

### Workshop

Walk through Dockerfile

## Dockerfile - best practices

Layer example

(Tomcat geeft een beetje veel layers, dus we gebruiken figlet)

```shell
#  Project dir
cd /home/cornet/code/prive/docker-deep-dive/figlet

docker build -t demo-app .
docker image history demo-app:latest
```

So, we have an image, what is next?

## Container Registry

Just a central place to store your images

Like in the old world you stored your jar file and its dependecies on a central place, e.g. Nexus, Artifactory.

Example: hub.docker.com/dirc


```shell
docker login

docker build -t dirc/demo-app:1.0 .
docker push dirc/demo-app:1.0
```

So, we have stored our image, what is next?

## Run your image


```shell
docker run -p 8080:80 nginx
```

So, we can run a single container, what is next?

## Multiple containers

### Network, storage

(compose example: https://docs.docker.com/compose/samples-for-compose/)

You can use the Docker CLI to create these (manually)

```shell
docker volume create demo-volume
docker network create demo-network
docker run --volume demo-volume --network demo-network app1
docker run --volume demo-volume --network demo-network app2
```

So, we can run multiple container, what is next?

### Declarative configuration

Docker Compose: declare containers using yaml

It is getting serious.

Repo:
- code
- Dockerfile
- docker-compose.yaml

Note: also usefull for running a single container!

Are we ready to run production?

So, we can declaratively run multiple containers, what is next?

### Replicas and resource limits

Warning: Psuedo code!
Just to get an idea

So, we can replicate multiple containers safely, what is next?

## Multiple nodes

- now it is getting serious
- production ready environments

We need: Orchestrators

### Container Orchestrators

If you gonna run container in production you need to choose one of these:

Order: Simple to advanced

So, we can run production now, what is next?

### Replicas / Autoscaling

Just mention existince

## Docker - what is it and what not

Can Bart answer?

Emphasize:
Orchestrators v.s. Docker containers
